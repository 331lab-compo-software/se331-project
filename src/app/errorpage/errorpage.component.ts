import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-errorpage',
  templateUrl: './errorpage.component.html',
  styleUrls: ['./errorpage.component.css']
})
export class ErrorpageComponent implements OnInit {
  guessIWillDie : boolean;
  constructor() { }

  ngOnInit() {
   this.guessIWillDie = Math.random() >= 0.5;
  }

}
