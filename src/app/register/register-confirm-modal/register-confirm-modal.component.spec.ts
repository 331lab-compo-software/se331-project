import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterConfirmModalComponent } from './register-confirm-modal.component';

describe('RegisterConfirmModalComponent', () => {
  let component: RegisterConfirmModalComponent;
  let fixture: ComponentFixture<RegisterConfirmModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterConfirmModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterConfirmModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
