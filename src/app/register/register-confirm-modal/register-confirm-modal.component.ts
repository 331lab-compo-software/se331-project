import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, EmailValidator } from '@angular/forms';
export interface ProfileDialogData {
  studentId: string;
  name: string;
  surname: string;
  dob: Date;
  email: string;
  password: string;
  image: File;

}
@Component({
  selector: 'app-register-confirm-modal',
  templateUrl: './register-confirm-modal.component.html',
  styleUrls: ['./register-confirm-modal.component.css']
})
export class RegisterConfirmModalComponent implements OnInit,AfterViewInit {
  
  constructor(
    public dialogRef: MatDialogRef<RegisterConfirmModalComponent>, 
    private fb:FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public data: ProfileDialogData
    ) {}

  form :any;
  imgsrc: string = 'assets/images/person-icon.png';
    
  ngOnInit(): void {
    //Set up the information preview
    this.setupForm();
  }
  ngAfterViewInit(): void {
    //Set the preview image
    this.setImage();
  }
  setupForm() {
    this.form = this.fb.group({
      studentId: [''],
      name: [''],
      surname: [''],
      dob: [''],
      email: [''],
      password: ['']
    });
  }

  setImage() {
    const imageFile = this.data.image;
    var fileReader = new FileReader();
    fileReader.onload = function() {
      var output = document.getElementById('preview');
      output.setAttribute("src",fileReader.result.toString())
    }
    fileReader.readAsDataURL(imageFile);
  }
    
  
  goBack(): void { this.dialogRef.close(false); }

  submit() {
    setTimeout( ()=> 
      this.dialogRef.close(true)
      ,300); 
    }
}



