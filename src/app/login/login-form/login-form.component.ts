import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/service/authentication-service.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
 authenticating:boolean;
  constructor(
    private authenService:AuthenticationService,
    private fb: FormBuilder,
    private router:Router,
    private route: ActivatedRoute
  ) { }

  form = this.fb.group({
   
    username: [''],
    password: ['']
  
  })
  returnUrl : string;
  isError = false;
  notApproved: boolean;
  fromHomePage: boolean;
  ngOnInit(): void { 
    // reset login status
    this.authenService.logout();
    this.isError = false;
    // get return url from route paramers or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.fromHomePage = (this.returnUrl === '/')? true : false;
    console.log('return url ' + this.returnUrl);
  }
  
  submit() {
    this.authenticating = true;
    const value = this.form.value;
    this.authenService.login(value.username,value.password)
      .subscribe(
        data => {
          this.authenticating = false;
          this.notApproved = false;
          console.log(data);
          this.isError = false;
          if (data.user.approved == true) {
              this.notApproved = false;
              if (this.returnUrl == '/') {
                this.loginRoute();
              }
              else {
                this.router.navigate([this.returnUrl]);
              }
          }
          else {
            this.authenService.logout();
            this.notApproved = true;
          }
        },
        error => {
          this.authenticating = false;
          this.isError = true;
        }
      );
    console.log(this.form.value);
  }

  private loginRoute() {
    if (this.authenService.hasRole('LECTURER')) {
      this.router.navigate(['/teacher/activitylist']);
      console.error("Going to teacher main page")
    }
    else if (this.authenService.hasRole('ADMIN')) {
      this.router.navigate(['/admin/activitylist']);
      console.error("Going to admin main page")
    }
    else if (this.authenService.hasRole('STUDENT')) {
      this.router.navigate(['/student/viewlist']);
      console.error("Going to student main page")
    }
  } 
}
  
  

