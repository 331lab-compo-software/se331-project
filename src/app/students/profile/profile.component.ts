import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { StudentService } from 'src/app/service/student-service';
import { AuthenticationService } from 'src/app/service/authentication-service.service';
import { FileUploadService } from 'src/app/service/file-upload.service';
import Student from 'src/app/entity/student';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private fb:FormBuilder, 
    private router:Router, 
    private studentService: StudentService,
    private authenService: AuthenticationService
    ) { }

    getStudentId() {
      return this.authenService.getCurrentUser().id;
    }
    form = this.fb.group({
      studentId: [''],
      name: [''],
      surname: [''],
      image: [''],
      dob: [''],
      email: [''],
    })
    student : Student;
     //Default image
  imgsrc: string = 'assets/images/person-icon.png';
  //Preview the image link
  previewImage() {
    this.imgsrc = this.form.get('image').value;
  }
  loading:boolean;
  ngOnInit():void {
    this.loading = true;
    this.route.params
    .subscribe((params: Params) => {
      this.studentService.getStudentProfile(this.getStudentId())
      .subscribe((inputStudent: Student) => {
        this.student = inputStudent
        this.form.get('studentId').patchValue(this.student.studentId);
        this.form.get('name').patchValue(this.student.name);
        this.form.get('surname').patchValue(this.student.surname);
        this.form.get('email').patchValue(this.student.email);
        this.form.get('dob').patchValue(this.student.dob);
        this.form.get('image').patchValue(this.student.image);
        console.log(this.student.dob);
        this.previewImage();
        this.loading = false;
      })
    });
   
  }

}
