import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTable } from '@angular/material';
import Activity from 'src/app/entity/activity';
import { StudentEnrolledListDatasource } from 'src/app/students/enrolled-activity-list/student-enrolled-list/student-enrolled-list-datasource';
import { BehaviorSubject, Observable } from 'rxjs';
import { ActivityService } from 'src/app/service/activity-service';
import Teacher from 'src/app/entity/teacher';
import { TeacherService } from 'src/app/service/teacher-service.service';
import Student from 'src/app/entity/student';
import { StudentService } from 'src/app/service/student-service';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthenticationService } from 'src/app/service/authentication-service.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-student-enrolled-list',
  templateUrl: './student-enrolled-list.component.html',
  styleUrls: ['./student-enrolled-list.component.css']
})
export class StudentEnrolledListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: StudentEnrolledListDatasource;
  
  displayedColumns = [
    'id',
    'name', 
    'location',
    'description',
    'hostTeacher',
    'regisPeriodStart',
    'regisPeriodEnd',
    'activityPeriodStart',
    'activityPeriodEnd',
    'pending',
    'withdraw'
  ]
  acivities: Activity[];
  students: Student[];
  enrolledStudentIds: number[];
  waitingStudentIds: number[];
  fetchedStudents: Observable<Student[]>;
  activity: Activity;

  filter: string;
  filter$: BehaviorSubject<any>;
  constructor(
    private activityService: ActivityService, 
    private teacherService: TeacherService,
    private authenService: AuthenticationService,
    private studentService: StudentService,
    private http: HttpClient) { }
  
    studentId : number;
    ngOnInit() {
      this.studentId = this.authenService.getCurrentUser().id;
      console.log(this.studentId);
    }

  // Date Filter 
  getDateRange(value) {
    // getting date from calendar
    const fromDate = value.fromDate;
    const toDate = value.toDate;

    console.log(fromDate, toDate);
    this.applyDateFilter(fromDate, toDate);
  }
  filterForm = new FormGroup({
    fromDate: new FormControl(),
    toDate: new FormControl(),
    name: new FormControl()
  });
  reset() {
    this.filterForm.get('fromDate').setValue(null);
    this.filterForm.get('toDate').setValue(null);
    this.filterForm.get('name').setValue('');
    this.applyBigFilter(null,null,'');
  }
  filterResult(value) {
    // getting date from calendar
    const fromDate = value.fromDate;
    const toDate = value.toDate;
    const name = value.name;

    console.log(fromDate, toDate,name);
    this.applyBigFilter(fromDate, toDate,name);
  }
  applyBigFilter(startDate : Date, endDate: Date, name:string) {
    console.log(startDate,endDate);
    if (name =='' || name==null) {
      name = '';
    }
    //No date
    if(startDate == ( null ) && endDate == ( null )) { 
        this.applyFilter(name); 
    }
    //Only start date
    else if(startDate == ( null ) && endDate != ( null )) {
      this.filter$.next([0,endDate,name]);
    }
    //Only end date
    else if(startDate != ( null ) && endDate == ( null )) {
      this.filter$.next([startDate,Infinity,name]);
    }
    //Both
    else {
      this.filter$.next([startDate,endDate,name]);
    }
  }

  pipe: DatePipe;


  get fromDate() { return this.filterForm.get('fromDate'); }
  get toDate() { return this.filterForm.get('toDate'); }

  alertM(value:any) {
    console.log(this.getDateRange(value));
  }

  applyDateFilter(startDate : Date, endDate: Date) {
    
    //No date
    console.log(startDate,endDate);
    if(startDate == ( null ) && endDate == ( null )) { 
      this.applyFilter(''); 
    }
    //Only start date
    else if(startDate == ( null ) && endDate != ( null )) {
      this.filter$.next([0,endDate]);
    }
    //Only end date
    else if(startDate != ( null ) && endDate == ( null )) {
      this.filter$.next([startDate,Infinity]);
    }
    //Both
    else {
      this.filter$.next([startDate,endDate]);
    }
  }

  ngAfterViewInit() {
    this.addData();
  }
  loading:boolean;
  addData() {
    this.loading = true;
    this.http.get<any>("http://35.173.243.253:8082/students/activities/"+this.studentId)
    .subscribe ( (data) => {
      var waitingAct = data.waitingActivities;
      var enrolledAct = data.enrolledActivities;
      var activities = waitingAct.concat(enrolledAct);
        this.dataSource = new StudentEnrolledListDatasource();
        this.dataSource.data = activities;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
        this.table.dataSource = this.dataSource;
        this.acivities = activities;
        this.loading = false;

      })
  }

  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  convertDate(date: string) { return eval(date); }

 
  isEnrolled(id: number) {
    return this.enrolledStudentIds.includes(id);
  }
  containWaiting(activity:Activity,id:number) {
    for (var i=0; activity.waitingStudents.length;i++) {
      if (activity.waitingStudents[i].id == id) {
        return true;
      }
      else return false;
    }
   }
   containEnrolled(activity:Activity,id:number) {
       for (var i=0; activity.enrolledStudents.length;i++) {
         if (activity.enrolledStudents[i].id == id) {
           return true;
         }
         else return false;
       }
   }
   enroll(activity:Activity) {
    if (confirm("Are you sure?"))  {
      this.http.get<Activity>(environment.activityApi+"/"+activity.id+"/enroll/"+this.studentId).subscribe(
        () => { this.addData(); }
      )  
    }
  }
  withdraw(activity:Activity) {
    if (confirm("Are you sure?"))  {
      this.http.get<Activity>(environment.activityApi+"/"+activity.id+"/withdraw/"+this.studentId).subscribe(
        () => { this.addData(); }
      )  
    }
  }

}
