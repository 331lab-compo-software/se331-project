import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { StudentService } from 'src/app/service/student-service';
import Student from 'src/app/entity/student';
import { MatDialog } from '@angular/material';
import { StudentUpdateModalComponent } from '../../student-update-modal/student-update-modal.component';
import { AuthenticationService } from 'src/app/service/authentication-service.service';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { FileUploadService } from 'src/app/service/file-upload.service';

@Component({
  selector: 'app-update-student',
  templateUrl: './update-student.component.html',
  styleUrls: ['./update-student.component.css']
})
export class UpdateStudentComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private fb:FormBuilder, 
    private router:Router, 
    private studentService: StudentService,
    private authenService: AuthenticationService,
    private fileUploadService:FileUploadService,) { }

  form = this.fb.group({
    //id: [''],
    studentId: [''],
    name: ['',Validators.compose([Validators.required])],
    surname: ['',Validators.compose([Validators.required])],
    image: [''],
    dob: [''],
    email: [''],
    password: [''],
    approved: ['']
  })

  validation_messages = {
    'name': [
      {type: 'required', message: 'Please enter your name'},
      {type: 'pattern', message: 'Name must contains 2 to 30 characters'}
    ],
    'surname': [
      { type: 'required', message: 'Please enter your surname'},
      {type: 'pattern', message: 'Surname must contains 2 to 30 characters'}
    ],
    'birth': [
      { type: 'required', message: 'Please enter your birthday'},
      { type: 'pattern', message: 'Birthday should be in dd/mm/yyyy'}
    ],
    'email': [
      { type: 'required', message: 'Please enter your email'},
      { type: 'pattern', message: 'Email not in a correct format'}
    ],
    'password': [
      { type: 'required', message: 'Please enter your password'},
      { type: 'pattern', message: 'Password must contains 5 to 15 characters'}
    ],
    'confirmPassword': [
      { type: 'required', message: 'Please enter your password'},
      { type: 'mustMatch', message: 'Passwords do not match'}
    ]
  };
  
  //Default image
  imgsrc: string = 'assets/images/person-icon.png';
    //Preview the image link
    previewImage() {
      this.imgsrc = this.form.get('image').value;
    }
    imgChange = false;
    loading : boolean;

  matchPassword(password:string,confirmPassword: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[password];
      const matchingControl = formGroup.controls[confirmPassword];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          // return if another validator has already found an error on the matchingControl
          return;
      }
      // set error on matchingControl if validation fails
      matchingControl.setErrors( control.value!==matchingControl.value? {mustMatch: true} : null);
    }
  }

  student : Student;

  //Boolean to check if the student wants to change password
  changePassword : boolean = false;
  changeStudentPassword() {
    this.changePassword = !this.changePassword
  }

 
  openDialog(): void {
    const dialogRef = this.dialog.open(StudentUpdateModalComponent, {
      width: '400px',
      data : { 
        username: this.form.get('email').value, 
        changePassword: this.changePassword}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (!result) {
        //Do nothing lol
      }
      else {
        if (result) {
          //if (result.newPassword) {
              this.form.get('password').setValue(result.newPassword);
              console.log(this.form.get('password').value);
        //}
        this.saveStudent();
      }
      }
    });
  }
  progress: number;
  uploadedUrl: string;
  //Save + attach img to student
  saveStudent() {
    
    const uploadedFile = this.selectedImgFile;
    this.progress = 0;
    if (this.imgChange) {
    this.fileUploadService.uploadFile(uploadedFile)
      .subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Sent:
            console.log('Request has been made!');
            break;
          case HttpEventType.ResponseHeader:
            console.log('Response header has been received!');
            break;
          case HttpEventType.UploadProgress:
            this.progress = Math.round(event.loaded / event.total * 100);
            console.log(`Uploaded! ${this.progress}%`);
            break;
          case HttpEventType.Response:
            console.log('User successfully created!', event.body);
            this.uploadedUrl = event.body;
            this.form.patchValue({
              image: this.uploadedUrl
            });
            console.error(this.form.value);
            this.previewImage();
            console.log(this.form.value);
            this.form.get('image').updateValueAndValidity();
            setTimeout(() => {
              this.progress = 0;
            }, 1500);
            this.studentService.saveStudentProfile(this.student.id,this.form.value).subscribe(
              (student) => {
              
                alert('Profile successfully updated!, please login again');
                this.router.navigate(['../login']);
               
              }, (error)=> { alert('There was an error updaing your information, please try later!');
            })
          }
        });
      }
      else {
        setTimeout(() => {
          this.progress = 0;
        }, 1500);
        this.studentService.saveStudentProfile(this.student.id,this.form.value).subscribe(
          (student) => {
          
            alert('Profile successfully updated!, please login again');
            this.router.navigate(['../login']);
           
          }, (error)=> { alert('There was an error updaing your information, please try later!');
        })
      }
  }
  selectedImgFile: File;
  //Preview image
  onSelectedFilesChanged(files?: FileList) {
    this.imgChange = true;
    const uploadedFile = files.item(0);
    var fileReader  = new FileReader();
    fileReader.onload = function() {
      var output = document.getElementById('previewImg');
      output.setAttribute("src",fileReader.result.toString())
    }
    fileReader.readAsDataURL(uploadedFile);
    this.selectedImgFile = uploadedFile;
  }

  submit() {
    this.openDialog();
  }
  getStudentId() {
    return this.authenService.getCurrentUser().id;
  }
  ngOnInit():void {
    this.loading = true;
    this.route.params
    .subscribe((params: Params) => {
      this.studentService.getStudentProfile(this.getStudentId())
      .subscribe((inputStudent: Student) => {
        this.student = inputStudent
        this.form.get('studentId').patchValue(this.student.studentId);
        this.form.get('name').patchValue(this.student.name);
        this.form.get('surname').patchValue(this.student.surname);
        this.form.get('email').patchValue(this.student.email);
        this.form.get('dob').patchValue(this.student.dob);
        //this.form.get('password').patchValue(this.student.password);
        this.form.get('image').patchValue(this.student.image);
        console.log(this.student.dob);
        this.previewImage();
        this.loading = false;
      })
    });
   
  }

}
