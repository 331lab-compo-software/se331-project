import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTable } from '@angular/material';
import Activity from 'src/app/entity/activity';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from 'src/app/service/activity-service';
import { ViewActivityListDatasource } from './view-activity-list-datasource';
import { TeacherService } from 'src/app/service/teacher-service.service';
import Teacher from 'src/app/entity/teacher';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from 'src/app/service/authentication-service.service';
import { environment } from 'src/environments/environment';
import Student from 'src/app/entity/student';
import  Comment  from 'src/app/entity/comment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-activity-list',
  templateUrl: './view-activity-list.component.html',
  styleUrls: ['./view-activity-list.component.css']
})
export class ViewActivityListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: ViewActivityListDatasource;

  displayedColumns = ['id', 'name', 'location', 'description', 'hostTeacher', 'regisPeriodStart', 'regisPeriodEnd', 'activityPeriodStart', 'activityPeriodEnd', 'details', 'enroll']
  acivities: Activity[];
  filter: string;
  filter$: BehaviorSubject<any>;
  activity: Activity;
  loading:boolean;

  constructor(
    private activityService: ActivityService, 
    private authenService: AuthenticationService,
    private http: HttpClient,
    private router: Router) { }
  hostTeachers: Teacher[];
  studentId : number;
  ngOnInit() {
    this.studentId = this.authenService.getCurrentUser().id;
    console.log(this.studentId);
  }
  // Date Filter 
  getDateRange(value) {
    // getting date from calendar
    const fromDate = value.fromDate;
    const toDate = value.toDate;

    console.log(fromDate, toDate);
    this.applyDateFilter(fromDate, toDate);
  }

  pipe: DatePipe;

  filterForm = new FormGroup({
    fromDate: new FormControl(),
    toDate: new FormControl(),
    name: new FormControl()
  });
  filterResult(value) {
    // getting date from calendar
    const fromDate = value.fromDate;
    const toDate = value.toDate;
    const name = value.name;

    console.log(fromDate, toDate,name);
    this.applyBigFilter(fromDate, toDate,name);
  }
  reset() {
    this.filterForm.get('fromDate').setValue(null);
    this.filterForm.get('toDate').setValue(null);
    this.filterForm.get('name').setValue('');
    this.applyBigFilter(null,null,'');
  }
  applyBigFilter(startDate : Date, endDate: Date, name:string) {
    console.log(startDate,endDate);
    if (name =='' || name==null) {
      name = '';
    }
    //No date
    if(startDate == ( null ) && endDate == ( null )) { 
        this.applyFilter(name); 
    }
    //Only start date
    else if(startDate == ( null ) && endDate != ( null )) {
      this.filter$.next([0,endDate,name]);
    }
    //Only end date
    else if(startDate != ( null ) && endDate == ( null )) {
      this.filter$.next([startDate,Infinity,name]);
    }
    //Both
    else {
      this.filter$.next([startDate,endDate,name]);
    }
  }


  get fromDate() { return this.filterForm.get('fromDate'); }
  get toDate() { return this.filterForm.get('toDate'); }

  alertM(value:any) {
    console.log(this.getDateRange(value));
  }

  applyDateFilter(startDate : Date, endDate: Date) {
    
    //No date
    console.log(startDate,endDate);
    if(startDate == ( null ) && endDate == ( null )) { 
      this.applyFilter(''); 
    }
    //Only start date
    else if(startDate == ( null ) && endDate != ( null )) {
      this.filter$.next([0,endDate]);
    }
    //Only end date
    else if(startDate != ( null ) && endDate == ( null )) {
      this.filter$.next([startDate,Infinity]);
    }
    //Both
    else {
      this.filter$.next([startDate,endDate]);
    }
  }

  ngAfterViewInit() {
    this.addData();
  }

  addData() {
    this.loading = true;
    this.activityService.getActivityList()
      .subscribe(activities => {
        this.dataSource = new ViewActivityListDatasource();
        this.dataSource.data = activities;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
        this.table.dataSource = this.dataSource;
        this.acivities = activities;
        this.loading = false;
      })
  }
  containWaiting(activity:Activity,id:number) {
   for (var i=0; activity.waitingStudents.length;i++) {
     if (activity.waitingStudents[0].id == id) {
       return true;
     }
     else return false;
   }
  }
  containEnrolled(activity:Activity,id:number) {
      for (var i=0; activity.enrolledStudents.length;i++) {
        if (activity.enrolledStudents[0].id == id) {
          return true;
        }
        else return false;
      }
  }

  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  convertDate(date: string) { return eval(date); }


  enroll(activity:Activity) {
    if (confirm("Are you sure you want to enroll into "+activity.name+"?"))  {
      this.http.get<Activity>(environment.activityApi+"/"+activity.id+"/enroll/"+this.studentId).subscribe(
        () => { this.addData(); }
      )  
    }
  }
  withdraw(activity:Activity) {
    if (confirm("Are you sure you want to withdraw from "+activity.name+"?"))  {
      this.http.get<Activity>(environment.activityApi+"/"+activity.id+"/withdraw/"+this.studentId).subscribe(
        () => { this.addData(); }
      )  
    }
  }

  routeToDetails(activityId: number) {
    this.router.navigate(['student/activityInfo',activityId]);
  }

}
