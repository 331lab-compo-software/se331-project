import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivityService } from 'src/app/service/activity-service';
import Teacher from 'src/app/entity/teacher';
import { TeacherService } from 'src/app/service/teacher-service.service';
import Activity from 'src/app/entity/activity';

@Component({
  selector: 'app-add-activity',
  templateUrl: './add-activity.component.html',
  styleUrls: ['./add-activity.component.css']
})
export class AddActivityComponent implements OnInit {
  constructor(private fb:FormBuilder,private router:Router,private activityService: ActivityService,private teacherService :TeacherService) {}
  form = this.fb.group({
    id: [''],
    name: ['',Validators.compose([Validators.required])],
    location: ['',Validators.compose([Validators.required])],
    description: ['',Validators.compose([Validators.required])],
    regisPeriodStart: [''],
    regisPeriodEnd: [''],
    activityPeriodStart: [''],
    activityPeriodEnd: [''],
    hostTeacher: ['']
    
  })
   private activityStartTime = { hour: 9, minute: 0, meriden: 'AM', format: 24 };
   private activityEndTime = { hour: 14, minute: 0, meriden: 'PM', format: 24 };
   onChangeactivityStartTime(event) {
    this.activityStartTime = event;
  }
  onChangeactivityEndTime(event) {
    this.activityEndTime = event;
  }
  loading:boolean;

   validation_messages = {
    'name': [
      {type: 'required', message: 'Please enter the activity name'}
    ],
    'location': [
      { type: 'required', message: 'Please enter the activity location'}
    ],
    'description': [
      { type: 'required', message: 'Please enter the activity description'}
    ],
   
  };
  //Combine date and time into one
  private combineTime(activity:any) {
    var aT = this.activityStartTime;
    var eT = this.activityEndTime;
    var startActDate = this.form.get('activityPeriodStart').value +" "+ aT.hour+":"+aT.minute+":00";
    var endActDate = this.form.get('activityPeriodEnd').value +" "+ eT.hour+":"+eT.minute+":00";
    activity.activityPeriodStart = (startActDate);
    activity.activityPeriodEnd = (endActDate);
    //this.form.patchValue( { activityPeriodStart: startActDate });
    //this.form.patchValue( { activityPeriodEnd: endActDate });
    return activity;
  }
  hostId: number;

  //Available teachers to host the activity
  hostTeachers : Teacher[];
  //Must convert form to actual object, just in case
  submit() {
    var newActivity = this.form.value;
    newActivity.host = new Teacher();
    newActivity.host.id = this.hostId;
    newActivity = this.combineTime(newActivity);
    
   
    this.activityService.saveActivity(newActivity).subscribe(
      (activity) => {
        alert('A new activity has been added!')
        
        this.router.navigate(['admin/activitylist']);
      }, (error)=> { alert('could not save value');
    })
  }

  ngOnInit() {
    this.loading= true;
    //Add teachers
    this.teacherService.getTeachers().subscribe(
      (teachers) => {
        this.hostTeachers = teachers;
        this.loading = false;
      }
    )
  }

  quickSetup() {
    this.form.patchValue({name:'Hello world'});
    this.form.patchValue({location:"CAMT"});
    this.form.patchValue({description:"No one read description"});
    //These will be fixed when we change Activity date
    this.form.patchValue({regisPeriodStart:new Date(2019,11,8).toISOString().split('T')[0]});
    this.form.patchValue({regisPeriodEnd:new Date(2019,11,22).toISOString().split('T')[0]});
    this.form.patchValue({activityPeriodStart:new Date(2020,0,1).toISOString().split('T')[0]});
    this.form.patchValue({activityPeriodEnd:new Date(2020,0,1).toISOString().split('T')[0]});
  }

}
