import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { AdminStudentTableDataSource } from './admin-student-table-datasource';
import { BehaviorSubject, of, Observable } from 'rxjs';
import Student from 'src/app/entity/student';
import { StudentService } from 'src/app/service/student-service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-admin-student-table',
  templateUrl: './admin-student-table.component.html',
  styleUrls: ['./admin-student-table.component.css']
})
export class AdminStudentTableComponent implements AfterViewInit,OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Student>;
  dataSource: AdminStudentTableDataSource;

/** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
displayedColumns = [
  'id',
  'studentId',
  'name',
  'surname',
  'email',
  //'password',
  'dob',
  'image',
  'approval',
  //'reject'
];
students: Student[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(
    private studentService: StudentService,
    private http: HttpClient) { }
  getStudentProfile(): Observable<any> {
    return this.http.get<any[]>
    ("http://35.173.243.253:8082/studentUsers");
  }
  ngOnInit() {
    //Don't do shit;
  }
  loading:boolean;
  addData() {
    this.loading = true;
    this.getStudentProfile()
    //this.studentService.getStudents()
    .subscribe(students => {
      setTimeout(
        () => {
      this.dataSource = new AdminStudentTableDataSource();
      this.dataSource.data = students;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;
      this.table.dataSource = this.dataSource;
      this.students = students;
      this.loading = false;
    },0
    )
  })
 
}

  ngAfterViewInit() {
    this.addData();
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }
  
  //Accept newly registered student
  accept(student:Student) {
    student.approved = true;
    this.http.put<Student>(
      "http://35.173.243.253:8082/studentUsers/"+student.id+"/enable",student
    ).subscribe(()=> {
    this.addData();
    })
  }
  //Reject newly registered student
  reject(student:Student) {
    student.approved = false;
    this.http.put<Student>(
      "http://35.173.243.253:8082/studentUsers/"+student.id+"/disable",student
    ).subscribe(()=> {
      this.addData();
      })

   
  }
    
   

}