import { Component, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication-service.service';

@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css']
})
export class MyNavComponent {
  @ViewChild('sidenav', {static:false}) sidenav: any;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private authenticationService: AuthenticationService,private breakpointObserver: BreakpointObserver,private router : Router) {}
      onTeacherPage() {
        return this.router.url.startsWith("/teacher");
      }
      onStudentPage() {
        return this.router.url.startsWith("/student");
      }
      onAdminPage() {
        return this.router.url.startsWith("/admin");
      }
      notLogin() {
        return this.router.url.startsWith("/login") || this.router.url.startsWith("/register");
      }
      logout() {
        this.authenticationService.logout();
      }
      
}
