import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import { StudentService } from 'src/app/service/student-service';
import { AuthenticationService } from 'src/app/service/authentication-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-nav-new',
  templateUrl: './my-nav-new.component.html',
  styleUrls: ['./my-nav-new.component.css']
})
export class MyNavNewComponent implements OnInit {
  ngOnInit(): void {
    
  }

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  defaultImageUrl = 'assets/camt_icon.png';

constructor(private breakpointObserver: BreakpointObserver, 
  private studentService: StudentService,
  private authService: AuthenticationService,
  private router: Router) {}

logout() {
  this.router.navigate(['']);
  this.authService.logout();
}
hasRole(role: string) {
  return this.authService.hasRole(role);
}
  getRole(role:string) {
    if(this.hasRole('ADMIN')) {
      return "Admin";
    }
    if(this.hasRole('LECTURER')) {
      return "Lecturer";
    }
    if(this.hasRole('STUDENT')) {
      return "Student";
    }
  }
get user() {
  return this.authService.getCurrentUser();
}
}
