import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ActivityService } from '../service/activity-service';
import { TeacherService } from '../service/teacher-service.service';
import Activity from '../entity/activity';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import Teacher from '../entity/teacher';
import { AuthenticationService } from '../service/authentication-service.service';
import { HttpClient, HttpEvent, HttpEventType } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CommentService } from '../service/comment.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { FileUploadService } from '../service/file-upload.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-activity-info-page',
  templateUrl: './activity-info-page.component.html',
  styleUrls: ['./activity-info-page.component.css']
})
export class ActivityInfoPageComponent implements OnInit {

  constructor(private route: ActivatedRoute, private activityService: ActivityService,
    private commentService: CommentService, private authenService: AuthenticationService, private http: HttpClient, private changeDetech: ChangeDetectorRef, private fb: FormBuilder,
    private fileUploadService: FileUploadService, private location:Location) { }
  form = this.fb.group({
    id: [''],
    image: [''],
    text: [''],
    time: [''],
    poster: ['']
  });
  activity: Activity;
  hostTeachers: Teacher[];
  activityId: number;
  currentUserId: number;
  defaultImageUrl = 'assets/images/person-icon.png';
  commentSize: number;
  comments: Comment[];
loading:boolean;
  ngOnInit(): void {
   
    this.getData();
  }
  backtoActivityListPage() {
    this.location.back();
  }
  textNotEmpty() : boolean {
    if (this.form.get('text').value.size > 0 || this.form.get('text') != undefined ) {
      return true;
    }
    return false;
  }

  getData() {
    this.loading =true;
    this.currentUserId = this.authenService.getCurrentUser().id;
    console.log("currentUserId :" + this.currentUserId);
    this.route.params
      .subscribe((params: Params) => {
        this.activityService.getActivity(+params['id'])
          .subscribe((inputActivity: any) => {
            this.activity = inputActivity;
            console.log(this.activity);
            this.commentSize = this.activity.comments.length;
            console.log("Comment size" + this.commentSize);
            this.comments = this.activity.comments;
            console.log(this.comments);
            this.loading = false;
          })
      });
  }

  get user() {
    return this.authenService.getCurrentUser();
  }

  hasRole(roles: string[]) {
    for (var i = 0; i < roles.length; i++) {
      if (this.authenService.hasRole(roles[i])) {
        return true;
      }
    }
    return false;

  }

  checkCommentSize() {
    if (this.commentSize === 0) {
      return true;
    } else {
      return false;
    }
  }

  refresh() {
    this.route.params
      .subscribe((params: Params) => {
        this.activityService.getActivity(+params['id'])
          .subscribe((inputActivity: any) => {
            this.activity = inputActivity;
            console.log(this.activity);
            this.commentSize = this.activity.comments.length;
            console.log("Comment size" + this.commentSize);
            this.comments = this.activity.comments;
            console.log(this.comments);
          })
      });
  }

  removeComment(id: number) {
    console.log("clicked id" + id);
    if (confirm("Are you sure you want to delete this comment?")) {
      this.commentService.deleteComment(id).subscribe();
      this.refresh();
      //this.comments.splice(0,1);
    }
    this.refresh();
  }




  //Upload image
  progress: number;
  uploadedUrl: string;
  imgsrc: string = 'assets/images/person-icon.png';
  selectedImgFile: File;


  onUploadClicked(files?: FileList) {
    console.log(typeof (files));
    console.log(files.item(0));
    const uploadedFile = files.item(0);
    this.progress = 0;
    this.fileUploadService.uploadFile(uploadedFile)
      .subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Sent:
            console.log('Request has been made!');
            break;
          case HttpEventType.ResponseHeader:
            console.log('Response header has been received!');
            break;
          case HttpEventType.UploadProgress:
            this.progress = Math.round(event.loaded / event.total * 100);
            console.log(`Uploaded! ${this.progress}%`);
            break;
          case HttpEventType.Response:
            console.log('User successfully created!', event.body);
            this.uploadedUrl = event.body;
            this.form.patchValue({
              image: this.uploadedUrl
            });
            this.previewImage();
            this.form.get('image').updateValueAndValidity();
            setTimeout(() => {
              this.progress = 0;
            }, 1500);
        }
      });
  }

  //Preview image
  onSelectedFilesChanged(files?: FileList) {
    const uploadedFile = files.item(0);
    var fileReader = new FileReader();
    fileReader.onload = function () {
      var output = document.getElementById('previewImg');
      output.setAttribute("src", fileReader.result.toString())
    }
    fileReader.readAsDataURL(uploadedFile);
    this.selectedImgFile = uploadedFile;
    this.refresh();
  }

  //Preview the image link
  previewImage() {
    this.imgsrc = this.form.get('image').value;
  }

  now: Date = new Date();
  submitComment() {
    //Messy time convertion code
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var minute = date.getMinutes();
    var second = date.getSeconds();
    var dateWithTimeInString = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;

    console.log("submit clicked")
    //console.log(this.form.value);
    var newComment = this.form.value;
    newComment.id = "1";
    const uploadedFile = this.selectedImgFile;

    newComment.image = "";
    newComment.poster = this.user;
    newComment.time = dateWithTimeInString;
    newComment.activity = new Activity();
    newComment.activity.id = this.activity.id;
    console.log(newComment);
    console.log(uploadedFile);
    if (uploadedFile != null) {
      this.fileUploadService.uploadFile(uploadedFile)
        .subscribe((event: HttpEvent<any>) => {
          switch (event.type) {
            case HttpEventType.Sent:
              console.log('Request has been made!');
              break;
            case HttpEventType.ResponseHeader:
              console.log('Response header has been received!');
              break;
            case HttpEventType.UploadProgress:
              this.progress = Math.round(event.loaded / event.total * 100);
              console.log(`Uploaded! ${this.progress}%`);
              break;
            case HttpEventType.Response:
              console.log('User successfully created!', event.body);
              this.uploadedUrl = event.body;
              this.form.patchValue({
                image: this.uploadedUrl
              });
              console.error(this.form.value);
              this.previewImage();
              console.log(this.form.value);
              this.form.get('image').updateValueAndValidity();
              newComment.image = this.form.get('image').value;
              setTimeout(() => {
                this.progress = 0;
              }, 1500);
              this.commentService.saveComment(newComment).subscribe(
                (comment) => {
                  alert('New comment has been')
                  this.refresh();
                }, (error) => {
                  alert('Unable to post comment');
                  this.refresh();
                }

              )
              
          }
        });
    } else {
      this.commentService.saveComment(newComment).subscribe(
        (comment) => {
          alert('New comment has been')
          this.refresh();
        }, (error) => {
          alert('Unable to post comment');
          this.refresh();
        }
        
      )
    
    }
  }

}
