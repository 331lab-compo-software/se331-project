import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityInfoPageComponent } from './activity-info-page.component';

describe('ActivityInfoPageComponent', () => {
  let component: ActivityInfoPageComponent;
  let fixture: ComponentFixture<ActivityInfoPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityInfoPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityInfoPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
