import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginFormComponent } from './login/login-form/login-form.component';
import { RegisterFormComponent } from './register/register-form/register-form.component';
import { TeacherActivityListComponent } from './teacher/teacher-activity-list/teacher-activity-list.component';
import { EditActivityComponent } from './teacher/edit-activity/edit-activity.component';
import { StudentactivityComponent } from './teacher/studentactivity/studentactivity.component';
import { AdminActivityListComponent } from './admin/activity-list/activity-list.component';
import { AddActivityComponent } from './admin/add-activity/add-activity.component';
import { AdminStudentTableComponent } from './admin/admin-student-table/admin-student-table.component';
import { UpdateStudentComponent } from './students/update-student/update-student/update-student.component';
import { StudentEnrolledListComponent } from './students/enrolled-activity-list/student-enrolled-list/student-enrolled-list.component';
import { ViewActivityListComponent } from './students/view-activity-list/view-activity-list.component';
import { ErrorpageComponent } from './errorpage/errorpage.component';
import { AuthGuard } from './guard/auth.guard';
import { ActivityInfoPageComponent } from './activity-info-page/activity-info-page.component';
import { ProfileComponent } from './students/profile/profile.component';


const systemRoutes: Routes = [
  {
    path: 'login',
    component: LoginFormComponent
  },
  {
    path: 'register',
    component: RegisterFormComponent
  },
  {
    path: 'teacher/activitylist',
    component: TeacherActivityListComponent,
    canActivate: [AuthGuard],
    data: { roles: ['LECTURER'] }
  },
  {
    path: 'teacher/editactivity/:id',
    component: EditActivityComponent,
    canActivate: [AuthGuard],
    data: { roles: ['LECTURER'] }
  },
  {
    path: 'teacher/studentactivity/:id',
    component: StudentactivityComponent,
    canActivate: [AuthGuard],
    data: { roles: ['LECTURER'] }
  },
  {
    path: 'admin/activitylist',
    component: AdminActivityListComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ADMIN'] }
  },
  {
    path: 'admin/addactivity',
    component: AddActivityComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ADMIN'] }
  },
  {
    path: 'admin/studentlist',
    component: AdminStudentTableComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ADMIN'] }
  },
  {
    path: 'student/profile',
    component: ProfileComponent,
    canActivate: [AuthGuard],
    data: { roles: ['STUDENT'] }
  },
  {
    path: 'student/update',
    component: UpdateStudentComponent,
    canActivate: [AuthGuard],
    data: { roles: ['STUDENT'] }
  },
  {
    path: 'student/enrolledlist',
    component: StudentEnrolledListComponent,
    canActivate: [AuthGuard],
    data: { roles: ['STUDENT'] }
  },
  {
    path: 'student/viewlist',
    component: ViewActivityListComponent,
    canActivate: [AuthGuard],
    data: { roles: ['STUDENT'] }
  },
  {
    path: 'admin/activityInfo/:id',
    component: ActivityInfoPageComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ADMIN'] }
  },
  {
    path: 'student/activityInfo/:id',
    component: ActivityInfoPageComponent,
    canActivate: [AuthGuard],
    data: { roles: ['STUDENT'] }
  },
  {
    path: 'teacher/activityInfo/:id',
    component: ActivityInfoPageComponent,
    canActivate: [AuthGuard],
    data: { roles: ['LECTURER'] }
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(systemRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class SystemRoutingModule {
}
