export default class Student {
    id: number;
    studentId: string;
    name: string;
    surname: string;
    image: string;
    dob: Date;
    email: string;
    password: string;
    approved: boolean; //If approved, they can use the system.
}
