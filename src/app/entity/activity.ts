import Student from './student';
import Teacher from './teacher';
export default class Activity {
    id: number;
    name: string;
    location: string;
    description: string;
    hostTeacher: Teacher; //Refer to teacher by id
    regisPeriodStart: Date;
    regisPeriodEnd: Date
    activityPeriodStart: Date;
    activityPeriodEnd: Date;
    waitingStudents: Student[];  //Refer to each student by id
    enrolledStudents: Student[]; //Refer to each student by id
    comments: Comment[];




}
