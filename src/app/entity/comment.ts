import Activity from './activity';

export default class Comment {
    id:number;
    image:string;
    text:string;
    time:Date;
    poster:any;
    activity:Activity;

}
