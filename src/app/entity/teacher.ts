export default class Teacher {
    id: number;
    name: string;
    surname: string; 
    email: string;
    password: string
}
