import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { TeacherActivityListDataSource } from './teacher-activity-list-datasource';
import { BehaviorSubject } from 'rxjs';
import Activity from 'src/app/entity/activity';
import { ActivityService } from 'src/app/service/activity-service';
import { RouterLink, Router } from '@angular/router';
import { TeacherService } from 'src/app/service/teacher-service.service';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthenticationService } from 'src/app/service/authentication-service.service';

@Component({
  selector: 'app-teacher-activity-list',
  templateUrl: './teacher-activity-list.component.html',
  styleUrls: ['./teacher-activity-list.component.css']
})
export class TeacherActivityListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: TeacherActivityListDataSource;

/** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
displayedColumns = ['id', 'name', 'location','hostTeacher','regisPeriodStart','regisPeriodEnd','activityPeriodStart','activityPeriodEnd','details','update','students'];
activities: Activity[];
  filter: string;
  filter$: BehaviorSubject<any>;
  constructor(
    private activityService: ActivityService,
    private router: Router,
    private teacherService: TeacherService,
    private authenService: AuthenticationService) {
 
   }
   getDateRange(value) {
    // getting date from calendar
    const fromDate = value.fromDate;
    const toDate = value.toDate;
  
    console.log(fromDate, toDate);
    this.applyDateFilter(fromDate,toDate);
  }

  pipe: DatePipe;

  filterForm = new FormGroup({
    fromDate: new FormControl(),
    toDate: new FormControl(),
    name: new FormControl()
  });
  reset() {
    this.filterForm.get('fromDate').setValue(null);
    this.filterForm.get('toDate').setValue(null);
    this.filterForm.get('name').setValue('');
    this.applyBigFilter(null,null,'');
  }
  filterResult(value) {
    // getting date from calendar
    const fromDate = value.fromDate;
    const toDate = value.toDate;
    const name = value.name;

    console.log(fromDate, toDate,name);
    this.applyBigFilter(fromDate, toDate,name);
  }
  applyBigFilter(startDate : Date, endDate: Date, name:string) {
    console.log(startDate,endDate);
    if (name =='' || name==null) {
      name = '';
    }
    //No date
    if(startDate == ( null ) && endDate == ( null )) { 
        this.applyFilter(name); 
    }
    //Only start date
    else if(startDate == ( null ) && endDate != ( null )) {
      this.filter$.next([0,endDate,name]);
    }
    //Only end date
    else if(startDate != ( null ) && endDate == ( null )) {
      this.filter$.next([startDate,Infinity,name]);
    }
    //Both
    else {
      this.filter$.next([startDate,endDate,name]);
    }
  }
  get fromDate() { return this.filterForm.get('fromDate'); }
  get toDate() { return this.filterForm.get('toDate'); }
  addData() {
    this.loading = true;
    this.activityService.getActivityListByTeacherId(
      this.authenService.getCurrentUser().id)
    .subscribe(activities =>  
      setTimeout(
        () => {
          //This page displays the actvities for teacher 1
      this.dataSource = new TeacherActivityListDataSource();
      this.dataSource.data = activities;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.filter$ = new BehaviorSubject<any>('');
      this.dataSource.filter$ = this.filter$;
      this.table.dataSource = this.dataSource;
      this.activities = activities;
      this.loading = false;
    }
    )
    ),0}
    loading:boolean;
  

  ngOnInit() {
    
  }
  
alertM(value:any) {
  console.log(this.getDateRange(value));
}
  ngAfterViewInit() {
    this.addData();
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }
  applyDateFilter(startDate : Date, endDate: Date) {
    
    //No date
    console.log(startDate,endDate);
    if(startDate == ( null ) && endDate == ( null )) { 
      this.applyFilter(''); 
    }
    //Only start date
    else if(startDate == ( null ) && endDate != ( null )) {
      this.filter$.next([0,endDate]);
    }
    //Only end date
    else if(startDate != ( null ) && endDate == ( null )) {
      this.filter$.next([startDate,Infinity]);
    }
    //Both
    else {
      this.filter$.next([startDate,endDate]);
    }
  }
 
  convertDate(date: string) {return eval(date); }

  routeToEditPage(actId: number) {
    this.router.navigate(['teacher/editactivity',actId]);
  }
  routeToStudentListPage(actId: number) {
    this.router.navigate(['teacher/studentactivity',actId]);
  }
  routeToDetails(activityId: number) {
    this.router.navigate(['teacher/activityInfo',activityId]);
  }
}
