import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { StudentactivityDataSource } from './studentactivity-datasource';
import Student from 'src/app/entity/student';
import { ActivityService } from 'src/app/service/activity-service';
import { BehaviorSubject, Observable } from 'rxjs';
import { StudentService } from 'src/app/service/student-service';
import { Route } from '@angular/compiler/src/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import Activity from 'src/app/entity/activity';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-studentactivity',
  templateUrl: './studentactivity.component.html',
  styleUrls: ['./studentactivity.component.css']
})
export class StudentactivityComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Student>;
  dataSource: StudentactivityDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'studentId', 'name', 'surname','image','approval', 'reject'];
  loading: boolean;
  activity: Activity;
    filter: string;
    filter$: BehaviorSubject<string>;
    constructor(
      private studentService: StudentService,
      private activityService: ActivityService,
      private route: ActivatedRoute,
      private router: Router,
      private http: HttpClient
      ) { }

      addData() {
        this.loading = true;
        this.route.params
        .subscribe((params: Params) => {
          
        this.activityService.getActivity(+params['id'])
        .subscribe(activity => {
          setTimeout(
            () => {
                var enrolledStudents = activity.enrolledStudents;
                var waitingStudents = activity.waitingStudents;
                var allStudentsInAct = 
                waitingStudents.concat(enrolledStudents);
          this.dataSource = new StudentactivityDataSource();
          this.dataSource.data = allStudentsInAct;
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.filter$ = new BehaviorSubject<string>('');
          this.dataSource.filter$ = this.filter$;
          this.table.dataSource = this.dataSource; 
          this.activity = activity;
          this.loading = false;
        },0)}
        )
      }
      )}
    
      ngOnInit(): void {
      
      }
  
    ngAfterViewInit() {
      this.addData();
    }
    applyFilter(filterValue: string) {
      this.filter$.next(filterValue.trim().toLowerCase());
    }
    isWaiting(studentId:number): boolean {
      for (var student of this.activity.waitingStudents) {
        if (student.id == studentId) {
          return true;
        }
      }
      return false;
    }
    isEnrolled(studentId:number): boolean {
      for (var student of this.activity.enrolledStudents) {
        if (student.id == studentId) {
          return true;
        }
      }
      return false;
    }


    withdraw(activity:Activity,studentId:number) {
      if (confirm("Are you sure?"))  {
        this.http.get<Activity>(environment.activityApi+"/"+activity.id+"/withdraw/"+studentId).subscribe(
          () => { this.addData(); }
        )  
      }
    }
    moveToEnroll(activity:Activity,studentId:number) {
      if (confirm("Are you sure?"))  {
        this.http.get<Activity>(environment.activityApi+"/"+activity.id+"/moveToEnrolled/"+studentId).subscribe(
          () => { this.addData(); }
        )  
      }
    }
    
    //Go back to activity list page
    backtoActivityListPage(){
      this.router.navigate(['teacher/activitylist']);
    }
  
  
  }
