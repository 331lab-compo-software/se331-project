import { Component, OnInit } from '@angular/core';
import { Params, Route, Router, ActivatedRoute } from '@angular/router';
import { ActivityService } from 'src/app/service/activity-service';
import Activity from 'src/app/entity/activity';
import { TeacherService } from 'src/app/service/teacher-service.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-edit-activity',
  templateUrl: './edit-activity.component.html',
  styleUrls: ['./edit-activity.component.css']
})
export class EditActivityComponent implements OnInit {
  constructor(private fb:FormBuilder,private route: ActivatedRoute,private router: Router, private activityService: ActivityService, private teacherService: TeacherService){}
  form = this.fb.group({
    name: ['',Validators.compose([Validators.required])],
    location: ['',Validators.compose([Validators.required])],
    description: ['',Validators.compose([Validators.required])],
    regisPeriodStart: [''],
    regisPeriodEnd: [''],
    activityPeriodStart: [''],
    activityPeriodEnd: [''],
    
  })

  validation_messages = {
    'name': [
      {type: 'required', message: 'Please enter the activity name'}
    ],
    'location': [
      { type: 'required', message: 'Please enter the activity location'}
    ],
    'description': [
      { type: 'required', message: 'Please enter the activity description'}
    ],
   
  };

  

loading : boolean;
  ngOnInit(): void {
    this.loading = true;
    this.route.params
     .subscribe((params: Params) => {
     this.activityService.getActivity(+params['id'])
     .subscribe((inputActivity: any) => 
     {
       this.activity = inputActivity;
       this.form.get('name').patchValue(this.activity.name);
       this.form.get('location').patchValue(this.activity.location);
       this.form.get('description').patchValue(this.activity.description);
       this.form.get('regisPeriodStart').patchValue(this.activity.regisPeriodStart);
       this.form.get('regisPeriodEnd').patchValue(this.activity.regisPeriodEnd);
       this.form.get('activityPeriodStart').patchValue(<string>inputActivity.activityPeriodStart.split(' ')[0]);
       this.form.get('activityPeriodEnd').patchValue(<string>inputActivity.activityPeriodEnd.split(' ')[0]);
        //Stupid thing here, regret using date for both day and time
        console.log(inputActivity.activityPeriodStart);
        var stDate = new Date(inputActivity.activityPeriodStart)
        var edDate = new Date(inputActivity.activityPeriodEnd)
        this.activityStartTime = { hour: stDate.getHours(), minute: stDate.getMinutes(), meriden: 'AM', format: 24 }
        this.activityEndTime = { hour: edDate.getHours(), minute: edDate.getMinutes(), meriden: 'PM', format: 24 };
        this.onChangeactivityStartTime(this.activityStartTime);
        this.onChangeactivityEndTime(this.activityEndTime)
        this.loading = false;
      }

     );
    
  })
}
private activityStartTime = { hour: 9, minute: 0, meriden: 'AM', format: 24 };
private activityEndTime = { hour: 14, minute: 0, meriden: 'PM', format: 24 };
onChangeactivityStartTime(event) {
 this.activityStartTime = event;
}
onChangeactivityEndTime(event) {
 this.activityEndTime = event;
}
exit() { this.router.navigate(['teacher/activitylist']); }
//Combine date and time into one
private combineTime(activity:any) {
  var aT = this.activityStartTime;
  var eT = this.activityEndTime;
  var startActDate = this.form.get('activityPeriodStart').value +" "+ aT.hour+":"+aT.minute+":00";
  var endActDate = this.form.get('activityPeriodEnd').value +" "+ eT.hour+":"+eT.minute+":00";
  activity.activityPeriodStart = (startActDate);
  activity.activityPeriodEnd = (endActDate);
  //this.form.patchValue( { activityPeriodStart: startActDate });
  //this.form.patchValue( { activityPeriodEnd: endActDate });
  return activity;
}
submit() {
  this.activity.name = this.form.get('name').value;
  this.activity.location = this.form.get('location').value;
  this.activity.description = this.form.get('description').value;
  this.activity.regisPeriodStart = this.form.get('regisPeriodStart').value;
  this.activity.regisPeriodEnd = this.form.get('regisPeriodEnd').value;
  this.activity.activityPeriodStart = this.form.get('activityPeriodStart').value;
  this.activity.activityPeriodEnd = this.form.get('activityPeriodEnd').value;
  
  this.activity = this.combineTime(this.activity);
  console.log(this.activity);
  this.activityService.saveEdit(this.activity).subscribe(
    (activity) => {
      alert('This activity has been updated!');
      
      this.router.navigate(['teacher/activitylist']);
    }, (error)=> { alert('could not save value');
  })
}
convertToDate(date: any) {
  if (date instanceof Date )
  return date.toISOString().split('T')[0];
  else return date;
}
  activity: Activity;
  
 
}
