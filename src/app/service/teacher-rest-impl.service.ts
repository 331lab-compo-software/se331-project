import { Injectable } from '@angular/core';
import { TeacherService } from './teacher-service.service';
import Teacher from '../entity/teacher';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TeacherRestImplService extends TeacherService {
  getTeachers(): Observable<Teacher[]> {
    return this.http.get<Teacher[]>(environment.lecturerApi);
  }
  getTeacher(id: number): Observable<Teacher> {
    return this.http.get<Teacher>(environment.lecturerApi+"/"+id);
  }
  getTeacherByEmailAndPassword(email: string, password: string): Observable<Teacher>{
    throw new Error("Method not implemented.");
  }

  constructor(private http: HttpClient) { super() }
}
