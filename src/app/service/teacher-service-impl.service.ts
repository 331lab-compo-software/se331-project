import { Injectable } from '@angular/core';
import Teacher from '../entity/teacher';
import { TeacherService } from './teacher-service.service';
import { Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class TeacherServiceImpl extends TeacherService {
  
  teachers: Teacher[] = [
    {
    'id': 1,
    'name': 'Chartchai',
    'surname': 'Doungsa-ard',
    'email': 'dto@cmu.ac.th',
    'password': 'dto'
    },
    {
      'id': 2,
      'name': 'Tui',
      'surname': 'Tui loves teaching 321',
      'email': 'tui@cmu.ac.th',
      'password': '321'
    }

  ]
  //Get all teachers in the DB
  getTeachers(): Observable<Teacher[]> {
    return of(this.teachers);
  }
  //Get a specific teacher by id
  getTeacher(id: number): Observable<Teacher> {
    const output: Teacher = this.teachers.find(teacher => teacher.id === id);
    return of(this.teachers[output.id - 1 ]);
  }
  //Get a specific teacher by email and password
  getTeacherByEmailAndPassword(email: string, password: string): Observable<Teacher> {
    const output: Teacher = this.teachers.find(teacher => teacher.email === email && teacher.password === password);
    return of(this.teachers[output.id - 1 ]);
  }

  constructor() {
    super();
  }
}
