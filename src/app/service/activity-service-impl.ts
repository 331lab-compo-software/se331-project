import { Injectable } from '@angular/core';
import Activity from '../entity/activity';
import Teacher from '../entity/teacher';
import { ActivityService }  from './activity-service';
import {Observable,of} from 'rxjs';
import {map} from 'rxjs/operators';
import { TeacherServiceImpl } from './teacher-service-impl.service';
import { StudentServiceImpl } from './student-service-impl.service';
@Injectable({
  providedIn: 'root'
})
export class ActivityServiceImpl extends ActivityService {

  activities : Activity[] = [ 
    /*{
    'id': 1,
    'name': "Loy Kratong",
    'location': "CAMT Fountain",
    'description': 'Enjoy Loy Kratong with international students',
    'hostTeacher': 1,
    'regisPeriodStart': new Date(2019,10,9),
    'regisPeriodEnd': new Date(2019,11,5),
    'activityPeriodStart': new Date(2019,11,6,8,0),
    'activityPeriodEnd': new Date(2019,11,6,14,0),
    'waitingStudents': [2],
    'enrolledStudents': [1]
  },
  {
    'id': 2,
    'name': "Testing",
    'location': "CAMT 5F",
    'description': 'Hello world',
    'hostTeacher': 1,
    'regisPeriodStart': new Date(2019,11,10),
    'regisPeriodEnd': new Date(2019,11,19),
    'activityPeriodStart': new Date(2020,1,2,8,0),
    'activityPeriodEnd': new Date(2020,1,4,18,0),
    'waitingStudents': [],
    'enrolledStudents': [1,2]
  }*/
  ]
  
  //Get all available acitivities
  getActivityList(): Observable<Activity[]> {
    return of(this.activities);
  }
  //Get a specific activity by id
  getActivity(id: number): Observable<Activity> {
    return of(this.activities[id - 1 ]);
    /*
    return of(this.activities)
    .pipe(map(activities => {
      const output: Activity = (activities as Activity[]).find(activity => activity.id === +id);
      return output;
    }));
    */
  }
  //Save activity into DB
  saveActivity(activity: Activity): Observable<Activity> {
    //activity.id =  this.activities.length + 1;
       
       this.getActivity(activity.id).subscribe( (act)=> {
        //If not in the DB yet, push 
        if (act == undefined) {
          activity.id =  this.activities.length + 1;
          console.log("ADDED");
          this.activities.push(activity);
         }
         else {
          console.log("REPLACED");
          this.activities[activity.id-1] = activity;
         }
       })
       /*
    if (this.getActivity(activity.id) == undefined) {
     
      activity.id =  this.activities.length + 1;
      console.log("ADDED");
      this.activities.push(activity);
    } //Else, just replace
    else {
      console.log("REPLACED");
      this.activities[activity.id-1] = activity;
    }
    */
    return of(activity);

  }
  saveEnrollActivity(id: number) {
    throw new Error("Method not implemented.");
  }
  check(id: number): boolean {
    throw new Error("Method not implemented.");
  }
  getEnrollActivity(): Observable<Activity[]> {
    throw new Error("Method not implemented.");
  }
  saveEdit(activity: Activity): Observable<Activity> {
    throw new Error("Method not implemented.");
  }
  saveUpdateActivity(id: number) {
    throw new Error("Method not implemented.");
  }

  //Get all student ids enrolled in an activity
  getEnrolledStudentsIdInActivity(id: number): number[] {
    var activity : Activity = this.getActivity(id-1)[0];
    console.log(activity);
    var studentIdList = activity.enrolledStudents;
    return null;//studentIdList;
    }
    
  
  //Get all student ids waiting to be enrolled in an activity
  getWaitingStudentsIdInActivity(id: number): number[] {
    var activity : Activity = this.getActivity(id)[0];
    var studentIdList = activity.waitingStudents;
    return null;//studentIdList;
  }

  getTeacherInActivity(id: number): Observable<Teacher[]> {
    throw new Error("Method not implemented.");
    
  }
  //Get all activities with the host teacher id
  getActivityListByTeacherId(id: number): Observable<Activity[]> {
    throw new Error("Method not implemented.");
    /*var activitiesForTeacher = [];
    for (var i=0; i< this.activities.length;i++) {
      if (this.activities[i].hostTeacher == id) {
        activitiesForTeacher.push(this.activities);
      }
    }
    return activitiesForTeacher;*/
    
  }

  constructor() {
    super();
  }
}
