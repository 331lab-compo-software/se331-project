import { Injectable } from '@angular/core';
import Activity from '../entity/activity';
import { Observable } from 'rxjs';
import Student from '../entity/student';
import Teacher from '../entity/teacher';
@Injectable({
  providedIn: 'root'
})
export abstract class ActivityService {

  abstract getActivityList(): Observable<Activity[]>;
  abstract getActivity(id: number): Observable<Activity>;
  abstract saveActivity(student: Activity): Observable<Activity>;
  abstract saveEnrollActivity(id: number);
  abstract check(id: number): boolean;
  abstract getEnrollActivity(): Observable<Activity[]>;
  abstract saveEdit(activity: Activity): Observable<Activity>;
  abstract saveUpdateActivity(id: number);
  abstract getEnrolledStudentsIdInActivity(id: number): number[];
  abstract getWaitingStudentsIdInActivity(id: number): number[];
  abstract getTeacherInActivity(id: number): Observable<Teacher[]>;
  abstract getActivityListByTeacherId(id:number): Observable<Activity[]>;
}
