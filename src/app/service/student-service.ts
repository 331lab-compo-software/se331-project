import { Injectable } from '@angular/core';
import  Student  from '../entity/student';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export abstract class StudentService {


  abstract getStudent(id: number): Observable<Student>;
  abstract getStudentByStudentId(studentId: string): Observable<Student>;
  abstract getStudentByEmail(email: string): Observable<Student>;
  abstract getStudents(): Observable<Student[]>;
  abstract saveStudent(student: Student): Observable<Student>;
  abstract getEnrollStudents(): Observable<Student[]>;
  abstract saveEnrollStudent(enrollStudent: Student): Observable<Student>;
  abstract getStudentByEmailAndPassword(email: string, password: string): Observable<Student>;
  abstract deleteStudent(id: number): Observable<Student>;
  abstract registerStudent(student: Student): Observable<Student>;
  abstract getStudentProfile(id: number): Observable<Student>;
  abstract saveStudentProfile(id: number,student: Student): Observable<Student>;
}
