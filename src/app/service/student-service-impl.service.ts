import { Injectable } from '@angular/core';
import  Student  from '../entity/student';
import { StudentService } from './student-service';
import { Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class StudentServiceImpl extends StudentService {
  registerStudent(student: Student): Observable<Student> {
    throw new Error("Method not implemented.");
  }
  getStudentProfile(id: number): Observable<Student> {
    throw new Error("Method not implemented.");
  }
  saveStudentProfile(id:number,student: Student): Observable<Student> {
    throw new Error("Method not implemented.");
  }
  //Get a specific student by studentId
  getStudentByStudentId(studentId: string): Observable<Student> {
    const output: Student = this.student.find(student => student.studentId === studentId);
    return of(this.student[output.id - 1 ]);
  }
  //Get a specific student by id
  getStudent(id: number): Observable<Student> {
    //const output: Student = this.student.find(student => student.id == id);
    return of(this.student[id - 1 ]);
  }
   //Get a specific student by email
  getStudentByEmail(email: string): Observable<Student> {
    const output: Student = this.student.find(student => student.email === email);
    return of(this.student[output.id - 1 ]);
  }
  //Get all students in the DB
  getStudents(): Observable<Student[]> {
    return of(this.student);
  }
  //Update or save students
  saveStudent(student: Student): Observable<Student> {
    var output : Student = this.student.find(stu => stu.id === student.id)
    //If not in the DB yet, push
    if (output == null) {
      student.id =  this.student.length + 1;
     // student.id =  this.student.length + 1;
      this.student.push(student);
      console.log("added")
    } //Else, just replace
    else {
      console.log("replaced")
      
      this.student[student.id-1] = student;
    }
    return of(student);
  }

  getEnrollStudents(): Observable<Student[]> {
    throw new Error("Method not implemented.");
  }
  saveEnrollStudent(enrollStudent: Student): Observable<Student> {
    throw new Error("Method not implemented.");
  }
  //Get a specific student by email and password
  getStudentByEmailAndPassword(email: string, password: string): Observable<Student> {
    const output: Student = this.student.find(student => student.email === email && student.password === password);
    return of(this.student[output.id - 1 ]);
  }
  //Delete a specific student
  deleteStudent(id: number): Observable<Student> {
     var output: Student = null;
    for (var i =0; i < this.student.length; i++)
   if (this.student[i].id === id) {
     output = this.student[i];
    this.student.splice(i,1);
      break;
   }
   return of(output);
  }
  student: Student[] = [{
    'id': 1,
    'studentId': 'Stu-01',
    'name': 'Jon',
    'surname': 'Tron',
    'image': 'https://pbs.twimg.com/profile_images/1089324240929779719/EG_cTZYk_400x400.jpg',
    'dob': new Date(1990,3,24),
    'email': 'jontron@cmu.ac.th',
    'password': '12345',
    'approved': true
  }, {
    'id': 2,
    'studentId': 'Stu-02',
    'name': 'PepsiMan',
    'surname': 'Gamer',
    'image': 'https://vignette.wikia.nocookie.net/advinmobius/images/5/58/Pepsi_man.png/revision/latest?cb=20180928182119',
    'dob': new Date(1999,3,4),
    'email': 'pepsiman@cmu.ac.th',
    'password': '54321',
    'approved': true
  },{
    'id': 3,
    'studentId': 'Stu-03',
    'name': 'Will',
    'surname': 'Smith',
    'image': 'https://www.biography.com/.image/t_share/MTE4MDAzNDEwNzQzMTY2NDc4/will-smith-9542165-1-402.jpg',
    'dob': new Date(1999,3,4),
    'email': 'wills@cmu.ac.th',
    'password': 'willsmith',
    'approved': false //Wait to be approved or else he can't login, if rejected then game over.
  },{
    'id': 4,
    'studentId': 'Stu-04',
    'name': 'Cat',
    'surname': 'Catt',
    'image': 'https://i.kym-cdn.com/photos/images/newsfeed/001/535/446/1c5.jpg',
    'dob': new Date(1998,5,4),
    'email': 'catz@cmu.ac.th',
    'password': 'catzzz',
    'approved': true
  }
  
];
  constructor() {
    super();
  }
}
