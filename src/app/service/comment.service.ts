import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export abstract class CommentService {

  abstract getComments(): Observable<Comment>;
  abstract getComment(id: number): Observable<Comment>;
  abstract saveComment(comment: Comment): Observable<Comment>;
  abstract deleteComment(id: number): Observable<Comment>;
}
