import { Injectable } from '@angular/core';
import  Student  from '../entity/student';
import { StudentService } from './student-service';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class StudentImplRestService extends StudentService {
  getStudentProfile(id: number): Observable<Student> {
    return this.http.get<Student>(environment.studentApi + "/profile/" + id);
  }
  saveStudentProfile(id:number,student: Student): Observable<Student> {
    return this.http.put<Student>(environment.studentApi + "/profile/" + id,student);
  }
  
  getStudent(id: number): Observable<Student> {
    return this.http.get<Student>(environment.studentApi + "/" + id);
  }
  getStudentByStudentId(studentId: string):Observable<Student> {
    throw new Error("Method not implemented.");
  }
  getStudentByEmail(email: string):Observable<Student> {
    throw new Error("Method not implemented.");
  }
  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>(environment.studentApi);
  }
  saveStudent(student:Student):Observable<Student> {
    return this.http.post<Student>(environment.studentApi,student);
  }
  getEnrollStudents(): Observable<Student[]> {
    throw new Error("Method not implemented.");
  }
  saveEnrollStudent(enrollStudent:Student): Observable<Student>{
    throw new Error("Method not implemented.");
  }
  getStudentByEmailAndPassword(email: string, password: string):Observable<Student> {
    throw new Error("Method not implemented.");
  }
  deleteStudent(id: number):Observable<Student> {
    return this.http.delete<Student>(environment.studentApi + "/" + id);
  }
  registerStudent(student:Student):Observable<Student> {
    return this.http.post<Student>(environment.registerApi,student);
  }

  constructor(private http: HttpClient) {
    super();
   }
}
