import { Injectable } from '@angular/core';
import { CommentService } from './comment.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentRestImplService extends CommentService{
  getComments(): Observable<Comment> {
    throw new Error("Method not implemented.");
  }
  getComment(id: number): Observable<Comment> {
    return this.http.get<Comment>(environment.commentApi + "/" + id);
  }
  saveComment(comment: Comment): Observable<Comment> {
    return this.http.post<Comment>(environment.commentApi, comment);
  }
  deleteComment(id: number):Observable<Comment> {
    return this.http.delete<Comment>(environment.commentApi + "/" + id);
  }

  constructor(private http: HttpClient) {
    super();
  }
}
