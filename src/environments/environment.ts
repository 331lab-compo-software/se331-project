// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  studentApi: 'http://35.173.243.253:8082/students',
  lecturerApi: 'http://35.173.243.253:8082/lecturers',
  activityApi: 'http://35.173.243.253:8082/activities',
  commentApi: 'http://35.173.243.253:8082/comments',
  //adminApi: 'http://localhost:8080/admins',
  uploadApi: 'http://35.173.243.253:8082/uploadFile',
  authenticationApi: 'http://35.173.243.253:8082/auth',
  registerApi: 'http://35.173.243.253:8082/registerStudent'
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
