# SE331Project <s>Midterm</s> Final

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.2.
## Group Name
    Wait, we have one?
## GitLab ID
    14329855
## Members
- 602115001
	Krittawit Khamjaroen
	email: krittawit_k@cmu.ac.th

- 602115023
    Somruk Laothamjinda
    email: somruk_laothamjinda@cmu.ac.th
    
## url to webpage
- <s>http://3.85.105.53:8081/login</s>
- http://35.173.243.253:8081/login
## Backend Gitlab
- https://gitlab.com/331lab-compo-software/se331-project-backend